﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UDataLayer.Context;
using UDataLayer.Context.Manager;
using UDataLayer.Context.Orm;

namespace WebApplication1.Controllers
{
    public class EstudianteController : Controller
    {
        Contexto contexto;
        public EstudianteController()
        {
            contexto = new Contexto();
        }



        // GET: Estudiante Index
        public ActionResult Index(string mensaje)
        {
            ViewBag.Mensaje = mensaje;
            return View(new UManager(contexto).ToSelectAll());
        }
        
        //public ActionResult Detalles(Guid? Id)
        //{
        //    if (Id.HasValue != null)
        //    {
        //        var estudiante = contexto.Estudiante.FirstOrDefault(x => x.Id == Id.HasValue);
        //        if (estudiante == null)
        //            return RedirectToAction("Index");
        //        else
        //            return View(estudiante);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index");
        //    }
        //}

        //[HttpPost]
        //public ActionResult Create(Estudiante estudiante)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var estudianteU = contexto.Estudiante.FirstOrDefault(x => x.Id == estudiante.Id);
        //        contexto.Estudiante.Add(estudiante);
        //        contexto.SaveChanges();

        //        return RedirectToAction("Index", new { mensaje = "Guardo correctamente" });
        //    }
        //    return View(estudiante);
        //}
    }
}