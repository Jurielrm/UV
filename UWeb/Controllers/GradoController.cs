﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UDataLayer.Context;
using UDataLayer.Context.Manager;

namespace WebApplication1.Controllers
{
    public class GradoController : Controller
    {
        Contexto contexto;
        public GradoController()
        {
            contexto = new Contexto();
        }
        
        
        // GET: Grado
        public ActionResult Index(string mensaje)
        {
            ViewBag.Mensaje = mensaje;
            return View(new UManager(contexto).SelesctGrado());
        }
    }
}