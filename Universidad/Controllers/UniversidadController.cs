﻿using Universidad.Context;
using Universidad.Context.Dao;
using Universidad.Models;
using Universidad.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Universidad.Controllers
{
    public class UniversidadController : Controller
    {
        Contexto contexto;
        public UniversidadController()
        {
            contexto = new Contexto();
        }

        // GET: Universidad
        public ActionResult Index(string mensaje)
        {
            ViewBag.Mensaje = mensaje;
            //Devuelve el model
            return View(new EstudianteDao(contexto).ToSelectForIndex());
        }

        // GET: Universidad/Details/5
        public ActionResult Detalles(long? id)
        {
            if (id.HasValue && id.Value > 0)
            {
                var estudiante = contexto.Estudiante.FirstOrDefault(x => x.Id == id.Value);
                if (estudiante == null)
                    return RedirectToAction("Index");
                else
                    return View(estudiante);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        // GET: Universidad/Crear un nuevo estudiante
        public ActionResult Create(long? id)
        {
            if (id.HasValue && id.Value > 0)
            {
                var estudiante = contexto.Estudiante.FirstOrDefault(x => x.Id == id.Value);
                if (estudiante == null)
                    return RedirectToAction("Index");
                else
                    return View(estudiante);
            }
            return View();
        }
        //CREAR Y EDITAR
        //// Envia POST: Universidad/Crear un nuevo estudiante
        //[HttpPost]
        //public ActionResult Create(Estudiante estudiante)
        //{
        //    if (estudiante.Id > 0)
        //    {
        //        var estudianteU = contexto.Estudiante.FirstOrDefault(x => x.Id == estudiante.Id);
        //        estudianteU.Nombre = estudiante.Nombre;
        //        estudianteU.FechaNacimiento = estudiante.FechaNacimiento;
        //        estudianteU.Domicilio = estudiante.Domicilio;
        //        contexto.SaveChanges();
        //        return RedirectToAction("Index", new { mensaje = "Actualizado" });
        //    }
        //    contexto.Estudiante.Add(estudiante);
        //    contexto.SaveChanges();

        //    return RedirectToAction("Index", new { mensaje = "Guardo correctamente" });
        //}

        // Envia POST: Universidad/Crear un nuevo estudiante
        [HttpPost]
        public ActionResult Create(Estudiante estudiante)
        {
            if (ModelState.IsValid)
            {
                var estudianteU = contexto.Estudiante.FirstOrDefault(x => x.Id == estudiante.Id);
                estudianteU.Nombre = estudiante.Nombre;
                estudianteU.FechaNacimiento = estudiante.FechaNacimiento;
                estudianteU.Domicilio = estudiante.Domicilio;
                contexto.Estudiante.Add(estudiante);
                contexto.SaveChanges();

                return RedirectToAction("Index", new { mensaje = "Guardo correctamente" });
            }
            return View(estudiante);
        }

        public ActionResult Editar(long? id)
        {
            if (id.HasValue && id.Value > 0)
            {
                var estudiante = contexto.Estudiante.FirstOrDefault(x => x.Id == id.Value);
                if (estudiante == null)
                    return RedirectToAction("Index");
                else
                    return View(estudiante);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Editar(Estudiante estudiante)
        {
            var estudianteU = contexto.Estudiante.FirstOrDefault(x => x.Id == estudiante.Id);
            estudianteU.Nombre = estudiante.Nombre;
            estudianteU.FechaNacimiento = estudiante.FechaNacimiento;
            estudianteU.Domicilio = estudiante.Domicilio;
            contexto.SaveChanges();

            return RedirectToAction("Index", new { mensaje = "Actualizado" });

        }

        // POST: Universidad/Delete/5
        [HttpPost]
        public ActionResult Delete(long? id)
        {
            if (id.HasValue && id.Value > 0)
            {
                var estudianteU = contexto.Estudiante.FirstOrDefault(x => x.Id == id.Value);
                if (estudianteU == null)
                    return RedirectToAction("Index", new { mensaje = "Estudiante no encontrado" });
                contexto.Estudiante.Remove(estudianteU);
                contexto.SaveChanges();
                return RedirectToAction("Index", new { mensaje = "Estudiante eliminado" });
            }
            return RedirectToAction("Index", new { mensaje = "Estudiante no encontrado" });
        }

        public ActionResult ENoEncotrado(long id)
        {
            ViewBag.Id = id;
            return View();
        }


        //[HttpGet]
        //[AllowAnonymous]
        //public JsonResult GetString()
        //{
        //    var dataTablesResult = new DataTablesResult();
        //    dataTablesResult.draw = 1;
        //    dataTablesResult.recordsTotal = 1000;
        //    dataTablesResult.recordsFiltered = 1000;
        //    dataTablesResult.data.AddRange(contexto.Estudiante.ToList());
        //    return Json(dataTablesResult, JsonRequestBehavior.AllowGet);
        //}

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetEstudiante(int draw, int start, int length)
        {
            var total = contexto.Estudiante.Count();

            var dataTablesResult = new DataTablesResult();
            dataTablesResult.draw = draw;
            dataTablesResult.recordsTotal = total;
            dataTablesResult.recordsFiltered = total;
            var EstudianteM = contexto.Estudiante.OrderBy(x => x.Id).Skip(start).Take(length).Select(e => new { Nombre = e.Nombre, FechaNacimiento = e.FechaNacimiento.ToString(), Domicilio = e.Domicilio, Id = e.Id }).ToList();

            //usuarioMostrar.ForEach(e =>
            //{
            //    e.DT_RowId = "row_" + e.Id;
            //    e.DT_RowData = new DT_RowData { pKey = (int)e.Id };
            //});
            dataTablesResult.data.AddRange(EstudianteM);
            return Json(dataTablesResult, JsonRequestBehavior.AllowGet);
        }
    }
}
