﻿using Universidad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Universidad.ViewModels;

namespace Universidad.Context.Dao
{
    public class EstudianteDao
    {
        Contexto _contexto;
        public EstudianteDao()
        {
            _contexto = new Contexto();
        }
        public EstudianteDao(Contexto contexto)
        {
            _contexto = contexto;
        }

        public List<EstudiantViewModels> ToSelectForIndex()
        {
            var lista = new List<EstudiantViewModels>();
            lista = _contexto.Estudiante.Select(e => new EstudiantViewModels
            {
                Id = e.Id,
                Nombre = e.Nombre,
                FechaNacimiento = e.FechaNacimiento,
                Domicilio = e.Domicilio,
            }).ToList(); ;
            return lista;
        }
    }
}