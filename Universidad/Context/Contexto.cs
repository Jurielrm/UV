﻿using Universidad.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Universidad.Context
{
    public class Contexto : DbContext
    {
        public DbSet<Estudiante> Estudiante { get; set; }
        public DbSet<Grado> Grado { get; set; }

        public Contexto() : base("Default")
        {

        }
    }
}