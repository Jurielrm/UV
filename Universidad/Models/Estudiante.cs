﻿using Universidad.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Universidad.Models
{
    public class Estudiante
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Debe ingresar un nombre")]
        [MaxLength(15)]
        [RegularExpression("^[a-z A-Z]*$", ErrorMessage = "Solo se permiten letras")]
        //[Display(Name = "NOMBRE COMPLETO")]
        public string Nombre { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required(ErrorMessage = "Debe ingresar la fecha de nacimiento")]
        [Column(TypeName = "date")]
        public DateTime FechaNacimiento { get; set; }
        [Required(ErrorMessage = "Debe ingresar el domicilio")]
        public string Domicilio { get; set; }


        public string GetDescripcion()
        {
            return Nombre + " - " + FechaNacimiento.ToShortDateString();
        }

        [NotMapped]
        public string DT_RowId { get; set; }
        [NotMapped]
        public DT_RowData DT_RowData { get; set; }
    }

    public class DT_RowData
    {
        public int pKey { get; set; }
    }
}