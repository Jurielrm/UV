﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Universidad.ViewModels
{
    public class DataTablesResult
    {
        public DataTablesResult()
        {
            data = new List<object>();
        }
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }

        public List<object> data { get; set; }
    }
}