﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Universidad.ViewModels
{
    public class EstudiantViewModels
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Domicilio { get; set; }

    }
}