﻿using System;
using System.Diagnostics;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UDataLayer.Context;
using UDataLayer.Context.Orm;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {

        //Contexto contexto;
        //public UnitTest1 ()
        //{
        //    contexto = new Contexto();
        //}
        private string cn = @"Data Source=DESKTOP-5GS9KH0\SQLEXPRESS;Initial Catalog=Universidad;Trusted_Connection=True;";
        [TestMethod]
        public void TestMethod1()
        {
            try
            {
                var c = new Contexto(cn);
                var e = new Estudiante
                {
                    Nombre = "Jaime Uriel",
                    Domicilio = "Lomas de Santa Fe"
                };
                var g = new Grado
                {
                    Semestre = "Semestre 1"
                };

                //e.Grado = g; //Relacionar estudiante con grado
                e.Grados.Add(g);
                c.Set<Estudiante>().Add(e);
                c.SaveChanges();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        //[TestMethod]
        //public void AsignaRelacion()
        //{
        //    var c = new Contexto(cn);
        //    var e = c.Set<Estudiante>().FirstOrDefault();
        //    e.GradoId = new Guid("4A2188F3-3CB2-4211-BB2E-E6FE960111DF");
        //    c.SaveChanges();
        //}

        [TestMethod]
        public void AsignaRelacion()
        {
            var c = new Contexto(cn);//C es la conexion
            var e = c.Set<Estudiante>().FirstOrDefault();//Devuelve el primer elemento de una secuencia de Estudiante
            e.Grados.Add(c.Set<Grado>().FirstOrDefault());
            c.SaveChanges();//Guarda
        }

        [TestMethod]
        public void SelectEstu()
        {
            var c = new Contexto(cn);
            c.Database.Log = memsaje => { Debug.WriteLine(memsaje); };
            var resultado = //Reseultado que es igual a contexto
                c.Set<Estudiante>() //Trae estudiante
                .Include(x => x.Grados)//Incluye Hrados
                .Where(x =>//
                    x.Nombre.Equals("Jaime Uriel"));
            var lista = resultado.ToList();//Lista es = resultado
            var listaEstudiante = c.Database.SqlQuery<Estudiante>("select * from Estudiante where Estudiante.nombre='Jaime'").ToList();
        }

        [TestMethod]
        public void LlenaEstudiante()
        {
            var c = new Contexto(cn);
            for (int e = 0; e < 1; e++)
            {
                var estudiante = new Estudiante
                {
                    Nombre = "Katia",
                    Domicilio = "CD Mexico"
                };
                estudiante.Grados.Add(new Grado
                {
                    Semestre = "Semestre 3"
                });

                c.Set<Estudiante>().Add(estudiante);
                c.SaveChanges();
            }
        }
    }
}
