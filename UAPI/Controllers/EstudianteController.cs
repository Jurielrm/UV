﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using UDataLayer.Context;
using UDataLayer.Context.Manager;
using UDataLayer.Context.Orm;

namespace UAPI.Controllers
{
    [RoutePrefix("Api/Estudiante")]
    public class EstudianteController : ApiController
    {
        public Contexto c = new Contexto(@"Data Source=DESKTOP-5GS9KH0\SQLEXPRESS;Initial Catalog=Universidad;Trusted_Connection=True;");
        private UManager uManager;
        //GET API/Values
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IEnumerable<Estudiante> Get()//Trea la lista de estudiante
        {
            uManager = new UManager(c);
            return uManager.ToSelectAll();//Regresa
        }
    }
}
