﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UDataLayer.Context;
using UDataLayer.Context.Manager;
using UDataLayer.Context.Orm;

namespace UAPI.Controllers
{
    public class ValuesController : ApiController
    {
        public Contexto c = new Contexto(@"Data Source=DESKTOP-5GS9KH0\SQLEXPRESS;Initial Catalog=Universidad;Trusted_Connection=True;");
        private UManager uManager;
        // GET api/values
        public IEnumerable<Estudiante> Get()
        {
            uManager = new UManager(c);
            return uManager.ToSelectAll();
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
