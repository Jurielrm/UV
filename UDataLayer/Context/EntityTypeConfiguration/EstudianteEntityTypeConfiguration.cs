﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDataLayer.Context.Orm;

namespace UDataLayer.Context.EntityTypeConfiguration
{
    public class EstudianteEntityTypeConfiguration:EntityTypeConfiguration<Estudiante>
    {
        public EstudianteEntityTypeConfiguration()
        {
            ToTable("Estudiante");
            HasKey(k =>k.Id);
            Property(p => p.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(p => p.Nombre).HasMaxLength(100).IsRequired();
            Property(p => p.FechaNacimiento).HasColumnType("Date").IsOptional();
            Property(p => p.Domicilio).HasMaxLength(200).IsRequired();
            //HasOptional(p => p.Grado).WithMany().HasForeignKey(g =>g.GradoId);
            HasMany(p =>p.Grados);
        }
    }
}
