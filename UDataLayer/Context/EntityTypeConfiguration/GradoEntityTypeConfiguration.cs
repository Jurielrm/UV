﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDataLayer.Context.Orm;

namespace UDataLayer.Context.EntityTypeConfiguration
{
    public class GradoEntityTypeConfiguration:EntityTypeConfiguration<Grado>
    {
        public GradoEntityTypeConfiguration()
        {
            ToTable("Grado");
            HasKey(k => k.Id);
            Property(p => p.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(p => p.Semestre).IsMaxLength().IsOptional();
        }
    }
}
