﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDataLayer.Context.Orm;

namespace UDataLayer.Context.Manager
{
    public class UManager
    {
        Contexto _context;
        public UManager()
        {
            _context = new Contexto("DefaultConnection"); 
        }
        //public UManager(Contexto contexto)
        //{
        //    _context = contexto;
        //}
        public UManager(Contexto c)
        {
            _context = c;
        }

        public List<Estudiante> ToSelectAll()
        {
            return _context.Set<Estudiante>().ToList();
        }
        public List<Grado> SelesctGrado()
        {
            return _context.Set<Grado>().ToList();
        }
    }
}
