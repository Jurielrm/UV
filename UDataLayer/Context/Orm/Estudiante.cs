﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDataLayer.Context.Orm
{
    public class Estudiante
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Domicilio { get; set; }

        public virtual ICollection<Grado> Grados { get; set; }//Genera un coleccion de Grado
        public string Combo => Id.ToString() + "-" + Nombre;
        
        public Estudiante()
        {
            Grados = new List<Grado>();//Instanciamos una lista de Grado
        }
        //public Guid? GradoId { get; set; }
        //public virtual Grado Grado { get; set; }

    }
}
