﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using UDataLayer.Context.EntityTypeConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDataLayer.Context.Orm;

namespace UDataLayer.Context
{
    public class Contexto : DbContext
    {
        public Contexto() : base("DefaultConnection")
        {
        }

        public Contexto(string nameOrConnectionString) : base(nameOrConnectionString) { }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EstudianteEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new GradoEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
